import { Context } from '@nuxt/types'
import { Store } from 'vuex'

export default function (ctx: Context) {
  if (!getToken(ctx.store)) {
    ctx.redirect('/login')
  }
}

export function getToken (store: Store<any>): string | null {
  let token = store.state.auth.token
  if (!token && process.client) {
    token = localStorage.getItem('theevent_acces_token')
    store.commit('auth/setToken', token)
  }
  return token
}

export function setToken (store: Store<any>, token: string) {
  store.commit('auth/setToken', token)
  if (process.client) {
    localStorage.setItem('theevent_acces_token', token)
  }
}
