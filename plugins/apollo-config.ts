import { Context } from '@nuxt/types'
import { ApolloLink } from 'apollo-link'
import { getToken } from '~/middleware/auth'

export default (context: Context) => {
  const cleanTypeName = new ApolloLink((operation, forward) => {
    if (operation.variables) {
      const omitTypename = (key: any, value: any) => (key === '__typename' ? undefined : value)
      operation.variables = JSON.parse(JSON.stringify(operation.variables), omitTypename)
    }
    return forward(operation).map((data) => {
      return data
    })
  })
  return {
    link: cleanTypeName,
    httpEndpoint: process.env.GRAPHQL_URL,
    getAuth: () => {
      return 'Bearer ' + getToken(context.store)
    }
  }
}
