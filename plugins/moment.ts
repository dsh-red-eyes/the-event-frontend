import { Context } from '@nuxt/types'

export default ({ app, $moment }: Context) => {
  $moment.locale(app.i18n.locale)

  app.i18n.onLanguageSwitched = (_oldLocale: string, newLocale: string) => {
    $moment.locale(newLocale)
  }
}
