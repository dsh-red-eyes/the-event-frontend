import { Context } from '@nuxt/types'
import { Store } from 'vuex'
import { TranslateResult } from 'vue-i18n'
import { UCAppBar } from '../types/appbar'

class UCAppBarImpl implements UCAppBar {
  private $store: Store<any>

  constructor (context: Context) {
    this.$store = context.store
  }

  setTitle (title: TranslateResult): void {
    this.$store.commit('appbar/setTitle', title)
  }

  setSearchHandler (h: (searchString: string) => void): void {
    this.$store.commit('appbar/setSearchHandler', h)
  }
}

export default (context: Context, inject: Function) => {
  const appbar = new UCAppBarImpl(context)
  inject('appbar', appbar)
}
