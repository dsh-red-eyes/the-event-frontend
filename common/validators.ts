export function getRules (vm: Vue): any {
  return {
    required: (value: string | null) => !!value || vm.$t('common.requiredField'),
    min: (value: string) => (!!value && value.length >= 8) || vm.$t('login.min8'),
    email: (value: string) => {
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return (!!value && re.test(value.toLowerCase())) || vm.$t('login.invalidEmailFormat')
    },
    matches: (value: string) => value === (vm as any).password || vm.$t('login.passwordNotMatch')
  }
}
