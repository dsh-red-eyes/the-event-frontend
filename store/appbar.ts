export const state = () => ({
  title: '',
  searchHandler: null,
  searchActive: false
})

export const mutations = {
  setTitle (state: any, title: string): void {
    state.title = title
  },
  setSearchHandler (state: any, h: ((searchString: string) => void) | null | undefined): void {
    state.searchHandler = h
    state.searchActive = false
  },
  setActive (state: any, active: boolean): void {
    state.searchActive = active
  }
}
