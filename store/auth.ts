export const state = () => ({
  token: null,
  me: {},
  users: []
})

export const mutations = {
  setToken (state: any, token: string): void {
    state.token = token
  },
  me (state: any, me: any): void {
    state.me = me
  },
  setUsers (state: any, users: any[]): void {
    state.users = users
  }
}
