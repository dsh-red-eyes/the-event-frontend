#!/bin/sh
if [ $CI_COMMIT_REF_NAME = "master" ]; then
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    export IMAGE_NAME=$CI_REGISTRY/dsh-red-eyes/the-event-frontend/the-event-frontend
    export VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
    docker build -t $IMAGE_NAME:$VERSION .
    docker push $IMAGE_NAME:$VERSION
    docker tag $IMAGE_NAME:$VERSION $IMAGE_NAME:latest
    docker push $IMAGE_NAME:latest
fi