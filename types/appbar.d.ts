import Vue from 'vue'
import { Context } from '@nuxt/types'
import { TranslateResult } from 'vue-i18n'

declare module 'vue/types/vue' {
  interface Vue {
    $appbar: UCAppBar
  }
}

declare module '@nuxt/types' {
  interface Context {
    $appbar: UCAppBar
  }
}

export interface UCAppBar {
  setTitle(title: TranslateResult): void
  setSearchHandler(h: ((searchString: string) => void) | null | undefined): void
}
